# Projeto Demo

## Intro

Este eh um repositorio git de um projeto demo

## Getting Started

### Build

```
docker build -t jenciso/website .
```

### Run

```
docker run --rm -p 8080:80 jenciso/website
```

### Test

```
curl -vs http://localhost:8080
```

## Contribute

...

## Author

Juan Enciso - [juan.enciso@gmail.com](mailto:juan.enciso@gmail.com)
